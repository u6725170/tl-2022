# Tl 2022


## Team Charter
   * [Team Charter](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EdgZyKmCU0pNvThvyq-XnrYBzwgjYMSw-ZDKzfP4BB9SWA?e=cvoidh)
   * [Team Ability](https://anu365-my.sharepoint.com/:x:/g/personal/u5927645_anu_edu_au/EfoD-jPYGDVNpEVBupOqqzwBs6I78roQ9eQ6eKsckpDYjA?e=mUawdP)

## Project Plan

   * [Statement of work](https://anu365-my.sharepoint.com/:b:/g/personal/u5927645_anu_edu_au/EZjY23pwardDkl2NxIgFCFMB_MmGhF0k2RYsi5OpOKntAg?e=6JEc7i)  
   * [Problematisation Analysis](https://anu365-my.sharepoint.com/:f:/g/personal/u5927645_anu_edu_au/EoB8hB7qMdVHpsWVTakbRYkBahJqs4Y0hepPeo_QtJXa2A?e=NHskhp)
   * [Timeline](https://anu365-my.sharepoint.com/:x:/g/personal/u5927645_anu_edu_au/EfoD-jPYGDVNpEVBupOqqzwBs6I78roQ9eQ6eKsckpDYjA?e=mUawdP)  
   * [Decision log and risk registrator](https://anu365-my.sharepoint.com/:x:/g/personal/u5927645_anu_edu_au/EfoD-jPYGDVNpEVBupOqqzwBs6I78roQ9eQ6eKsckpDYjA?e=YZH3tY) 


## Audits

   * [Audit 1 slides](https://anu365-my.sharepoint.com/:p:/g/personal/u5927645_anu_edu_au/EYc6za03S4pClk9eMTHNOvcB2QC6Gx41s_bpIIfs6hjLUg?e=iXiw39)
     

## Agendas 

   * [Saturday 05 Feb 2022 (Week 2)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/Efy-lei0UfNFhK5NmsQM8bwBkWAy0zKaa9iqbMLg40pGTg?e=Zv1XXm)
   * [Sunday 06 Mar 2022 (week 2)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EcwQ4rTNaVtLvVQRu-o7XgkBp-5bznB8HEjs5bi6S5sFkA?e=iU7Gbo)
   * [Tuesday 08 Mar 2022 (week 3) - First Audit](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/Ef5sUtccwYBMmWeIpyfQkjsBp4mwfoieev4Da5y4qyPAng?e=KUXrFW)
   * [Saturday 12 Mar 2022 (Week 3) ](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EU0E1K80SCtMunm6j079YqYBJz3f7ndWI3iEIXMP_lDLSw?e=OFheI0)

## Meeting Minutes

   * [Team Formation Minute, Thursday 24 Feb 2022  (Week 1)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EeehCqNYLRpJlByeDZyaVBkBDr_mIrI-szngngV9jIxn6Q?e=dSy6hd)      
   * [Initiation Meeting Minute, Friday 25 Feb 2022  (Week 1) ](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EQeir2MdKMxHg2OuUP_WkxMB37awlRtFr-ayhO6ZfiItBA?e=Na8DEk)  
   * [Friday 04 Mar 2022 (Week 2)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EcwHUcNiWy1Gn1gDok9EDSgBolOY8NRC1eL4wxGUtWX1Gw?e=RWFLEh)
   * [Saturday 05 Mar 2022 (Week 2)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EQeir2MdKMxHg2OuUP_WkxMB37awlRtFr-ayhO6ZfiItBA?e=dPfWEw)
   * [Sunday 06 Mar 2022 (Week 2)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/Edoa7Sxgc-dCkwzKzZ2qEZwBlcmB5PtroIc8cqAVqL5aDQ?e=ZOFned)
   * [Saturday 12 Mar 2022 (Week 3)](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/EUBuiOnl92tOlQ5OZAdXccsBWOEi3fAnpeiDwiWcr7V0Kg?e=t5UINM)

## Others
   * [IP Agreement](https://anu365-my.sharepoint.com/:w:/g/personal/u5927645_anu_edu_au/ERXZt0kOYidFu1PPGyZku24BYR1JwJRa2SUFOp2dypw4ow?e=9bZvDl)
   
